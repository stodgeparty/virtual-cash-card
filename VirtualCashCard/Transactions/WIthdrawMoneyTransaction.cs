﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCashCard.Transactions
{
    public class WithdrawMoneyTransaction : ITransaction
    {
        public decimal Amount { get; }
        public int Pin { get; }

        public WithdrawMoneyTransaction(decimal amount, int pin)
        {
            Amount = amount;
            Pin = pin;
        }

        public void Post(Card card)
        {
            if (!card.ValidatePin(Pin))
            {
                throw new ArgumentException("Invalid Pin", nameof(Pin));
            }

            // TODO: amount validation (> 0, new balance > 0 etc)
            card.Balance -= Amount;
        }
    }
}
