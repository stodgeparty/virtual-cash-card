﻿using System;
using NUnit.Framework;
using VirtualCashCard.Transactions;

namespace VirtualCashCard.Tests
{
    [TestFixture]
    public class TopUpTransactionTests
    {
        private decimal _balance;
        private decimal _topUpAmount;
        private Card _card;

        [SetUp]
        public void Setup()
        {
            _balance = 70m;
            _topUpAmount = 50m;
            _card = new Card(_balance);
        }

        [Test]
        public void WithdrawMoney_IncreasesBalance()
        {
            var transaction = new TopupTransaction(_topUpAmount);

            _card.PostTransaction(transaction);

            Assert.AreEqual(120m, _card.Balance);
        }
    }
}
