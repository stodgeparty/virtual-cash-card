﻿using System;
using NUnit.Framework;

namespace VirtualCashCard.Tests
{
    [TestFixture]
    public class CardPinManagementTests
    {
        private Card _card;

        [SetUp]
        public void Setup()
        {
            _card = new Card();
        }

        [Test]
        public void SetPin_LessThan1000_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _card.SetPin(999), "Pin must be 4 digits");
        }

        [Test]
        public void SetPin_MoreThan9999_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _card.SetPin(999), "Pin must be 4 digits");
        }

        [Test]
        public void SetPin_WithValidValue_AndValidateWithCorrectPin_ReturnsTrue()
        {
            var pin = 1234;
            _card.SetPin(pin);

            var isValid = _card.ValidatePin(pin);

            Assert.IsTrue(isValid);
        }

        [Test]
        public void SetPin_WithValidValue_AndValidateWithInCorrectPin_ReturnsTrue()
        {
            var pin = 1234;
            _card.SetPin(pin);

            var isValid = _card.ValidatePin(pin+1);

            Assert.IsFalse(isValid);
        }
    }
}
