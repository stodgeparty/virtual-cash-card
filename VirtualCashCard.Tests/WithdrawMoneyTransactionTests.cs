﻿using System;
using NUnit.Framework;
using VirtualCashCard.Transactions;

namespace VirtualCashCard.Tests
{
    [TestFixture]
    public class WithdrawMoneyTransactionTests
    {
        private decimal _balance;
        private decimal _withdrawalAmount;
        private int _pin;
        private Card _card;

        [SetUp]
        public void Setup()
        {
            _balance = 70m;
            _withdrawalAmount = 50m;
            _pin = 1234;
            _card = new Card(_balance);
            _card.SetPin(_pin);
        }

        [Test]
        public void WithdrawMoney_WithValidPin_ReducesBalance()
        {
            var transaction = new WithdrawMoneyTransaction(_withdrawalAmount, _pin);

            _card.PostTransaction(transaction);

            Assert.AreEqual(20m, _card.Balance);
        }

        [Test]
        public void WithdrawMoney_WithInvalidPin_ThrowsArgumentException()
        {
            var transaction = new WithdrawMoneyTransaction(_withdrawalAmount, _pin - 1);

            Assert.Throws<ArgumentException>(() => _card.PostTransaction(transaction), "Invalid Pin");
        }
    }
}
