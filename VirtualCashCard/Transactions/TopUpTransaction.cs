﻿namespace VirtualCashCard.Transactions
{
    public class TopupTransaction : ITransaction
    {
        public decimal Amount { get; }

        public TopupTransaction(decimal amount)
        {
            Amount = amount;
        }

        public void Post(Card card)
        {
            // TODO: amount validation (> 0 etc)
            card.Balance += Amount;
        }
    }
}
