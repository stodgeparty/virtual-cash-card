﻿using NUnit.Framework;

namespace VirtualCashCard.Tests
{
    [TestFixture]
    public class CardTests
    {
        [Test]
        public void CanInstantiate()
        {
            var card = new Card();

            Assert.IsNotNull(card);
        }

        [Test]
        public void CanInstantiateWithBalance()
        {
            var balance = 200m;

            var card = new Card(balance);

            Assert.IsNotNull(card);
            Assert.AreEqual(balance, card.Balance);
        }
    }
}
