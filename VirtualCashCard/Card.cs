﻿using System;
using VirtualCashCard.Transactions;

namespace VirtualCashCard
{
    public class Card
    {
        private int _pin;
        private readonly object _syncLock = new object();

        public Card(decimal balance)
        {
            Balance = balance;
        }

        public Card()
        {
        }

        public decimal Balance { get; internal set;  }

        public void SetPin(int pin)
        {
            if (pin < 1000 || pin > 9999)
            {
                throw new ArgumentException("Pin must be 4 digits", nameof(pin));
            }
            _pin = pin;
        }

        public bool ValidatePin(int pin)
        {
            // TODO: extract secure Pin store...
            return _pin == pin;
        }

        public void PostTransaction(ITransaction transaction)
        {
            // TODO: extract to a transaction processor, strategies for each transaction
            lock (_syncLock)
            {
                transaction.Post(this);
            }
        }
    }
}
