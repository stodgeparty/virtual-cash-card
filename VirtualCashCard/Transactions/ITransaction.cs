﻿namespace VirtualCashCard.Transactions
{
    public interface ITransaction
    {
        void Post(Card card);
    }
}
